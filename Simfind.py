__all__ = ['Simfind']
__version__ = '1.2.0'
__author__ = 'Yao-Yuan Mao'

from collections import Mapping
from textwrap import TextWrapper
from json import load

def update_dict(d, u):
    for k, v in u.iteritems():
        d[k] = update_dict(d.get(k, {}), v) if isinstance(v, Mapping) else u[k]
    return d

class Simfind:
    def __init__(self):
        self.data = {}
        self.options = {}
        self.__dest_key = u'#!'
        self.__wrapper = TextWrapper(initial_indent='  ', \
                subsequent_indent='  ')
        self.interactive_prompt = True

    def addJson(self, json_file, reindex=True):
        with open(json_file) as f:
            update_dict(self.data, load(f))
        if reindex:
            self.indexOptions()

    def __addOptions(self, opt_str, dest_str):
        l = opt_str.split()
        if len(l) == 0: return
        cd = self.options
        for i in l:
            cd = cd.setdefault(i, {})
        cd[self.__dest_key] = dest_str

    def indexOptions(self):
        self.options = {}
        for k in self.data:
            self.__addOptions(k, k)
            for alias in self.data[k].get('alias', []):
                self.__addOptions(alias, k)

    def promptAvailableData(self):
        keys = list(self.data.viewkeys())
        keys.sort()
        print '--------------------------------------'
        print 'Available simulations: '
        print '--------------------------------------'
        for k in keys:
            print ' ', k

    def promptAvailableOptions(self, d=None):
        if d is None:
            d = self.options
        keys = list(d.viewkeys())
        keys.sort()
        lines = self.__wrapper.wrap('  '.join(keys))
        print '--------------------------------------'
        print 'Available options: '
        print '--------------------------------------'
        for l in lines:
            print l
    
    def get(self, key, item=None):
        if item is not None:
            return self.data.get(key, {}).get(item)
        else:
            return self.data.get(key)

    def query(self, keys, d=None, return_dict=False):
        if d is None: d = self.options 
        ret = d if return_dict else None
        if isinstance(keys, basestring): keys = keys.split()
        if len(keys) == 0: return ret
        key = keys[0].lower()
        options = filter(lambda k: k.lower().startswith(key), d.viewkeys())
        if len(options) == 1:
            option = options[0]
        elif len(options) > 1:
            try:
                i = [o.lower() for o in options].index(key)
            except ValueError:
                return ret
            else:
                option = options[i]
        else:
            return ret
        if self.__dest_key in d[option] and len(keys) == 1:
            return d[option][self.__dest_key]
        else: 
            res = self.query(keys[1:], d[option], return_dict)
            return res

    def queryInteractive(self, keys, d=None):
        res = self.query(keys, d, return_dict=True)
        if isinstance(res, basestring):
            d = self.get(res)
            print '--------------------------------------'
            print res
            print '--------------------------------------'
            for k in d:
                if k == 'alias':
                    continue
                out = ''
                if isinstance(d[k], Mapping):
                    for kk in d[k]:
                        out += '%s: %s, '%(kk, str(d[k][kk]))
                    out = out[:-2]
                else:
                    out = str(d[k])
                print '  %s\n    %s'%(k, out)
        else:
            self.promptInteractive(res)
    
    def promptInteractive(self, d=None):
        if d is None:
            self.promptAvailableData()
        else:
            self.promptAvailableOptions(d)
        if self.interactive_prompt:
            try:
                new_keys = raw_input('Enter option (ctrl-c to quit) >> ').split()
            except (EOFError, KeyboardInterrupt, SystemExit):
                print "quit"
                return
            if len(new_keys) > 0:
                self.queryInteractive(new_keys, d)
            else:
                self.promptInteractive(d)

